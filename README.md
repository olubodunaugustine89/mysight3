mySight 3
=======

mySight3 is http://myspectral.com Spectruino analyzer for light spectra in UV/VIS/NIR



## __Please read installation and getting started instructions on the mySight3 wiki:__

https://gitlab.com/myspectralcom/mysight3/wikis/home
